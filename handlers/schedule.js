module.exports.scheduleHandler= function(packet, client){
    const userid = packet.d.user_id;
    var roleName ="";
    switch (packet.d.emoji.name) {
        case "🌱":
            roleName="874347369989308426"
            break;    
        case "1️⃣":
            roleName="874347982609326150"
        break;   
        case "2️⃣":
            roleName="874348287237439509"
        break;   
        case "3️⃣":
            roleName="874348348843393025"
            break;   
        case "4️⃣":
            roleName="874348437997494273"
            break;   
        case "🔁":
            roleName="874348879871623269"
            break;   
        case "📚":
            roleName="874386564845813810"
            break;   
        case "📨":
            roleName="874348981042434128"
            break;   
        default:
            return;
    }
    if('MESSAGE_REACTION_ADD' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.id===roleName);
        var memberRole2= client.roles.cache.find(r=>r.id==="");
        let member = client.members.cache.get(userid)
        member.roles.add(memberRole);
        member.roles.add(memberRole2);
    } else if ( 'MESSAGE_REACTION_REMOVE' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.id===roleName);
        let member = client.members.cache.get(userid)
        member.roles.remove(memberRole);
        var count = 0;
        var ids = ["", "", "", "", "", "",""];
        ids.forEach(id => {            
            if(member.roles.cache.find(r=>r.id == id)  && roleName != id){
                count++;
            }
        });

        if(count == 0){
            var memberRole2= client.roles.cache.find(r=>r.id==="");
            member.roles.remove(memberRole2);
        }
    }
}