var {
    range
} = require('lodash');
const EorzeaWeather = require('eorzea-weather');
var EorzeaTime = require('eorzea-time');

const EIGHT_HOURS = 8 * 175 * 1000;
const ONE_DAY = EIGHT_HOURS * 3;

module.exports.weatherHandler = function (zone) {

    const eorzeaWeather = new EorzeaWeather(zone, {
        locale: "en"
    });

    if (eorzeaWeather.validate()) {
        const startTime = getStartTime(new Date()).getTime() ;//- ONE_DAY *2;
        const weathers = range(
            startTime,
            startTime + ONE_DAY * 10,
            EIGHT_HOURS,
        ).map((time) => {
            const startedAt = new Date(time);
            return {
                name: eorzeaWeather.getWeather(startedAt),
                startedAt,
            };
        });

        //832080676295409694

        return weathers;
    } else {
        return [];
    }
}

var getStartTime = (date) => {
    const oneHour = 175 * 1000;
    const msec = date.getTime();
    const bell = (msec / oneHour) % 24;
    const startMsec = msec - Math.round(oneHour * bell);
    return new Date(startMsec);
};

module.exports.weatherIconHandler = function (status) {

    var url = "";

    switch (status.toLowerCase()) {
        case "wind":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/b/bf/Wind.png";
            break;
        case "blizzard weather":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/7/72/Blizzard_%28weather%29.png";
            break;
        case "clear skies":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/f/fc/Clear_Skies.png";
            break;
        case "cloud":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/3/38/Clouds.png";
            break;
        case "dust storm":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/2/26/Dust_Storms.png";
            break;
        case "fair skies":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/2/29/Fair_Skies.png";
            break;
        case "dust storms":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/2/26/Dust_Storms.png";
            break;
        case "fog":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/e/ef/Fog.png";
            break;
        case "gales":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/d/d3/Gales.png";
            break;
        case "gloom":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/5/59/Gloom.png";
            break;
        case "heat wave":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/2/2d/Heat_Wave.png";
            break;
        case "rain":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/1/1d/Rain.png";
            break;
        case "sandstorms":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/2/2c/Sandstorms.png";
            break;
        case "snow":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/4/4e/Snow.png";
            break;
        case "thunder":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/f/fd/Thunder_%28weather%29.png";
            break;
        case "core radiation":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/0/06/Core_Radiation.png";
            break;
        case "gales garuda":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/7/75/Gales_%28Garuda%29.png";
            break;
        case "gloom king moogle mog":
            url = "https://ffxiv.consolegameswiki.com/mediawiki/images/f/f3/Gloom_%28King_Moggle_Mog%29.png";
            break;
        default:
            break;
    }
    return url;
}