module.exports.languageHandler= function(packet, client){
    const userid = packet.d.user_id;
    var roleName ="";
    switch (packet.d.emoji.name) {
        case "1️⃣":
            roleName="Japanese"
            break;    
        case "2️⃣":
            roleName="Chinese"
        break;   
        case "3️⃣":
            roleName="Thais"
        break;   
        case "4️⃣":
            roleName="Vietnamese"
            break;
        default:
            return;
    }

    if('MESSAGE_REACTION_ADD' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.name===roleName);
        let member = client.members.cache.get(userid)
        member.roles.add(memberRole);
    } else if ( 'MESSAGE_REACTION_REMOVE' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.name===roleName);
        let member = client.members.cache.get(userid)
        member.roles.remove(memberRole);
    }
}