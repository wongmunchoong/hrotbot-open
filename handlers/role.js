module.exports.roleHandler= function(packet, client){
    const userid = packet.d.user_id;
    var roleName ="";
    switch (packet.d.emoji.name) {
        case "Tank":
            roleName="Tanks"
            break;    
        case "Healer":
            roleName="Healers"
        break;   
        case "Melee":
            roleName="Melee DPS"
        break;   
        case "Caster":
            roleName="Caster DPS"
            break;   
        case "RDPS":
            roleName="Ranged DPS"
            break;   
        default:
            return;
    }
    if('MESSAGE_REACTION_ADD' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.name===roleName);
        let member = client.members.cache.get(userid)
        member.roles.add(memberRole);
    } else if ( 'MESSAGE_REACTION_REMOVE' == packet.t){
        var memberRole= client.roles.cache.find(r=>r.name===roleName);
        let member = client.members.cache.get(userid)
        member.roles.remove(memberRole);
    }
}