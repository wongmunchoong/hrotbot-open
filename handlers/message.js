const axios = require('axios');
const Discord = require('discord.js');
const e = require('express');
let idx = require('idx');
const { forEach } = require('lodash');
const { convertOne, convertAll } = require("string-apostrophes");


const tankEmoji ="829918310635143230";
const healerEmoji ="829918311185514537";
const meleeEmoji ="829918310648643605";
const rdpsEmoji ="829918311093108746";
const casterEmoji ="829918310740787251";

let memberService = require('../service/members');
module.exports.messageHandler= function(message, client){  
    if (message.author.bot) return;
    if(['', '',''].includes(message.channel.id)){ 
        message.react("🛎");    
    }

    if(message.channel.id === ""){     
        var data = message.content.toLowerCase().split(' ');
        if(data.length == 3){
            switch (data[0]) {
                case "aegis":
                    nickname(message, client, "Aegis", data[1], data[2]);
                    break;
                case "atomos":
                    nickname(message, client, "Atomos", data[1], data[2]);
                    break;           
                case "carbuncle":
                    nickname(message, client, "Carbuncle", data[1], data[2]);
                    break;           
                case "garuda":
                    nickname(message, client, "Garuda", data[1], data[2]);
                    break;           
                case "gungnir":
                    nickname(message, client, "Gungnir", data[1], data[2]);
                    break;           
                case "kujata":
                    nickname(message, client, "Kujata", data[1], data[2]);
                    break;           
                case "ramuh":
                    nickname(message, client, "Ramuh", data[1], data[2]);
                    break;           
                case "tonberry":
                    nickname(message, client, "Tonberry", data[1], data[2]);
                    break;           
                case "typhon":
                    nickname(message, client, "Typhon", data[1], data[2]);
                    break;           
                case "unicorn":
                    nickname(message, client, "Unicorn", data[1], data[2]);
                    break;       
                default:
                    try {
                        message.delete({ timeout: 100 });message
                        .channel
                        .send(`<@${message.author.id }> Hmm, a lost soul from ${data[0]}, this place is for Elemental Datacenter player unfortunately. Go now. go find your Datacenter instead.`) 
                        .then(msg => {
                            msg.delete({ timeout: 20000 })
                        });
                    } catch (error) {
                        console.log(error.message)
                    }
                    break;
            }
        } else {            
            try {
                message.delete({ timeout: 100 });
                message
                    .channel
                    .send(`<@${message.author.id }> Invalid Command.\n Please enter your information as follow \n[SERVER] [FIRST NAME] [LAST NAME]`) 
                    .then(msg => {
                        msg.delete({ timeout: 20000 })
                    });
            } catch (error) {
                console.log(error.message)
            }
        }
    } else 
    if(message.channel.type == "dm"){
        const embed = new Discord.MessageEmbed()
        .setColor('#edd028')
        .setDescription(message.content);

        client.channels.cache.get('')
        .send({content:`<@${message.author.id}> `, embed: embed});
    } else 
    if(message.channel.id === ""){ 
        var replyMessage = idx(message, _obj => _obj.reference.messageID);
        if(replyMessage){
            var predecessorMessage = client.channels.cache.get('')
            .messages.fetch({
                around: replyMessage,
                limit: 1
              })    .then(msg => {
                const fetchedMsg = msg.first();
                const user = fetchedMsg.mentions.users.first();
                if (user === undefined) {
                    return; // Do not proceed, there is no user.
                }
                client.members.cache.get(user.id).send(message.content);
              });

        } else {
            const Tbroadcast = "broadcast";
            const user = message.mentions.users.first();
            if(message.content.toLowerCase().startsWith(Tbroadcast)) {
                const roles = message.mentions.roles.array();
                if(roles.length > 0){
                    message.guild.roles.cache.get(roles[0].id).members.forEach(member => {                   
                            member.send(message.content);
                    });
                }
                return;
            }    

            if (user === undefined) {
                return; // Do not proceed, there is no user.
            }
            client.members.cache.get(user.id).send(message.content);

        }
    }else 
    if(message.channel.id === ""){
        const roles = message.mentions.roles.array();
        message.guild.roles.cache.get("").members.forEach(member => {
            var count = 0;
            roles.forEach(id => {            
                if(member.roles.cache.find(r=>r.id == id.id)  ){
                    count++;
                }
            });
            if(count>0){
                member.send(message.content);
            }
        });
    }else {
        
        var data = message.content.toLowerCase().split(' ');

        if(message.member.roles.cache.find(r=>r.id == "") 
        || message.member.roles.cache.find(r=>r.id == "") 
        || message.member.roles.cache.find(r=>r.id == "") ){
            const Tsearch = "search";
            if(message.content.toLowerCase().startsWith(Tsearch)) {
                var preT = message.content.slice(Tsearch.length).trim();
                const Tmember = "member";
                const Tuser = "user";
                const Tplayer = "player";
                if(preT.toLowerCase().startsWith(Tmember)) {
                    var postT = preT.slice(Tmember.length).trim();
                    searchUser(postT, message);
                } else if(preT.toLowerCase().startsWith(Tuser)) {
                    var postT = preT.slice(Tuser.length).trim();
                    searchUser(postT, message);
                } else if(preT.toLowerCase().startsWith( Tplayer)) {
                    var postT = preT.slice(Tplayer.length).trim();
                    searchUser(postT, message);
                } 
            }    
        }

        if(message.content.toLowerCase() == ('hrothbot')) {
             client.members.cache.get(message.author.id).send('Meow! Is there anything i can assist you? (PS: All inquiry, feedback and request will be made available to Moderators)');
             message.delete({ timeout: 10 })
        }

        if(message.content.toLowerCase() == ('meow')) {
            getMeow(message);
        }
        if(message.content.toLowerCase() == ('woof')) {
            getWoof(message);
        }
        if(message.content.toLowerCase() == ('twit')||message.content.toLowerCase() == ('chirp')) {
            getBirb(message);
        }
        if(message.content.toLowerCase() == ('fox')||message.content.toLowerCase() == ('ring ding ding')) {
            getFox(message);
        }
        if(message.content.toLowerCase() == ('panda')||message.content.toLowerCase() == ('chirp')) {
            getPanda(message);
        }
        if(message.content.toLowerCase() == ('suspanda')||message.content.toLowerCase() == ('redpanda')) {
            getRedPanda(message);
        }
        if(message.content.toLowerCase() == ('auspanda')||message.content.toLowerCase() == ('koala')) {
            getKoala(message);
        }
        if(message.content.toLowerCase() == ('bunny')) {
            getBunny(message);
        }
        if(message.content.toLowerCase() == ('quack')) {
            getQuack(message);
        }
    }
}

function searchUser(user, message){
    message.channel.startTyping();
    memberService.getList(user, (err,record) => {
        if(record.length >0){
            record.forEach(element => {
                message.channel.send(`${element.nickname ?? `${element.username}#${element.discriminator}`}  \`\` <@${element.userID}> \`\``);
            });
        } else {
            message.channel.send("Nobody found");
        }
        message.channel.stopTyping();
    });
}

function getQuack(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://random-d.uk/api/v2/random`,
    }).then(response => {   
        message.channel.send(response.data.url);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getFox(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://some-random-api.ml/img/fox`,
    }).then(response => {   
        message.channel.send(response.data.link);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getPanda(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://some-random-api.ml/img/panda`,
    }).then(response => {   
        message.channel.send(response.data.link);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getRedPanda(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://some-random-api.ml/img/red_panda`,
    }).then(response => {   
        message.channel.send(response.data.link);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getKoala(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://some-random-api.ml/img/koala`,
    }).then(response => {   
        message.channel.send(response.data.link);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getBirb(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://some-random-api.ml/img/birb`,
    }).then(response => {   
        message.channel.send(response.data.link);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getBunny(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://api.bunnies.io/v2/loop/random/?media=png,mp4`,
    }).then(response => {   
        message.channel.send(response.data.media.poster);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getWoof(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://dog.ceo/api/breeds/image/random`,
    }).then(response => {   
        message.channel.send(response.data.message);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getMeow(message){
    message.channel.startTyping();
    axios({
        method:'get',
        url:`https://cataas.com/cat?json=true`,
    }).then(response => {   
        message.channel.send(`https://cataas.com${response.data.url}`);
        message.channel.stopTyping();
    })
    .catch(error => {
        console.error(error);
        message.channel.stopTyping();
    });
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

function nickname(message, client, server, firstName, lastName){
    message.channel.startTyping();
    console.log(message.member.id);
    var uri = `https://xivapi.com/character/search?name=${encodeURI(firstName)}+${encodeURI(lastName)}&server=${server}&private_key=1dd03dbf13874ff69d77b247968838df4daa8624f1594b3db3851538049c7772`;
    axios({
        method:'get',
        url:uri,
    }).then(response => {
        console.log(`statusCode: ${response.status}`);        
        var validName = false;
        var user = null;
        if(response.data.Results.length>0){
            response.data.Results.forEach(element => {                
                var subjectA =convertAll(element.Name.toLowerCase(), {
                    convertApostrophes: true,
                    convertEntities: false,
                  }).result;         
                  var subjectB =convertAll( `${firstName} ${lastName}`, {
                      convertApostrophes: true,
                      convertEntities: false,
                    }).result;
                if(subjectA == subjectB){
                    validName = true
                    user = element;
                }
            });
        }

        if(validName && user) {
            memberService
                .getDetail({request:{lodestoneuserid:user.ID}}, (err,record) => {
                    if(idx(record, _obj => _obj.user.userID) == message.author.id || idx(record, _obj => _obj.user.userID) == null) {       
                        try {
                            message.delete({ timeout: 100 })
                        } catch (error) {
                            console.log(error.message)
                        }
                        var dcUser = message.member.user;
                        memberService.update({
                        request:{        
                            "guildID":idx(dcUser, _obj => _obj.id),
                            "deleted": false,
                            "avatarId":idx(dcUser, _obj => _obj.avatar),
                            "nickname":`${user.Name} (${server})`,
                            "userID":message.author.id,
                            "discriminator":idx(dcUser, _obj => _obj.discriminator) ,
                            "username": idx(dcUser, _obj => _obj.username) ,
                            "roles": idx(dcUser, _obj => _obj._roles) ,
                            lodestoneavatar:idx(user, _obj => _obj.Avatar) ,
                            lodestonepotrait:idx(user, _obj => _obj.Avatar).replace("c0_96x96", "l0_640x873"),
                            lodestoneserver:server,
                            lodestoneuserid:idx(user, _obj => _obj.ID),
                            lodestoneusername:idx(user, _obj => _obj.Name)
                        }}, (err,result) => {
                            const embedmessage = new Discord.MessageEmbed()
                                .setColor('#0099ff')
                                .setTitle(user.Name)
                                .setDescription(user.Server)
                                .setThumbnail(user.Avatar)
                                .setFooter('Successfully updated your nickname to above character', 'https://ffxiv.gamerescape.com/w/images/8/8b/Mainquest4_Icon.png');
                            try {                
                                message.member.setNickname(`${user.Name} (${server})`);
                            } catch (error) {
                                console.log(error.message)
                            }
                            var memberRole= client.roles.cache.find(r=>r.name==="Verified");
                            message.member.roles.add(memberRole);
                            message.channel.send(embedmessage).then(msg => { msg.delete({ timeout: 30000 }) });
                            message.channel.send(`Please Proceed to <#829748472713576538> to select your roles. If you can't see the <#829748472713576538>, please try register again. Thank you`).then(msg => { msg.delete({ timeout: 30000 }) });     
                            try {                
                                const embedmessage2 = new Discord.MessageEmbed()
                                    .setColor('#0099ff')
                                    .setTitle(user.Name)
                                    .setDescription(user.Server)
                                    .setThumbnail(user.Avatar)
                                    .setImage(user.Avatar.replace("c0_96x96", "l0_640x873"))
                                    .setFooter(
                                        `Register by: ${message.member.user.username}#${message.member.user.discriminator}`, 
                                        'https://ffxiv.gamerescape.com/w/images/6/6b/MainIcon43.png'
                                    );
                                client
                                    .channels
                                    .cache
                                    .get('829626591076024330')
                                    .send(embedmessage2);
                            } catch (error) {
                                console.log(error.message)
                            }
                            message.channel.stopTyping();
                        });
                    } else {
                        const embedmessage = new Discord.MessageEmbed()
                            .setColor('#CC3300')
                            .setTitle(user.Name)
                            .setDescription(user.Server)
                            .setThumbnail(user.Avatar)
                            .setFooter(
                                `This username has been taken by: ${idx(record, _obj => _obj.user.username)}#${idx(record, _obj => _obj.user.discriminator)}`, 
                                'https://ffxiv.gamerescape.com/w/images/7/79/Mainquest5_Icon.png'
                            );
                        message.channel.send(embedmessage);
                        message.channel.send(`Please Contact <@116580545328185348> for assistance`);
                        message.channel.stopTyping();
                    }
                });
        } else {
            try {
                message.delete({ timeout: 100 });
            } catch (error) {
                console.log(error.message)
            }
            message
                .channel
                .send(`<@${message.author.id }> Unable to identify your Name with Lodestone.\n Please enter your information as follow \n[SERVER] [FIRST NAME] [LAST NAME]`) 
                .then(msg => {
                    msg.delete({ timeout: 20000 })
                });
            message.channel.stopTyping();
        }
    })
    .catch(error => {
        console.error(error);        
        message.delete({ timeout: 100 })
        message.channel.send(error.response?.data?.Message).then(msg => { msg.delete({ timeout: 30000 }) });;
        message.channel.stopTyping();
    });
}