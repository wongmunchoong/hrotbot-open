const COLLECTION_NAME = 'members';
let _ = require('lodash');
let async = require('async');
let idx = require('idx');
var { DateTime } = require('luxon');

let database = require('../database');

module.exports = {
    getList: function (params, callback) {
		let collection = database.collection(COLLECTION_NAME);
        collection
        .find({ $or: [
             { username: {$regex : params, $options : 'i'}},
              { nickname: {$regex : params, $options : 'i'}}, 
              { lodestoneusername: {$regex : params, $options : 'i'}}
        ]})
        .limit(20)
        .toArray(function (err, results) {
			callback(err, results);
        });

    },
	getDetail: function (params, callback) {
		let collection = database.collection(COLLECTION_NAME);
		let status = [];
		let request = idx(params, _obj => _obj.request) || {};
		let filterKey = idx(params, _obj => _obj.omit) || [];
		if (Array.isArray(filterKey)) {
			filterKey.push('deleted');
			filterKey.push('resetLookup');
		}

		request = Object.assign(request, { deleted: false });
		collection.findOne(request, function (err, user) {
			if (err) {
				console.log('error running query', err);
			} else {				
				user = _.omit(user, filterKey);
			}
			callback(err, { user });
		});
	},
    create: function (params, callback) {
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }

        collection.insertOne(request, function (err, result) {
            let event = {};
            if (err) {
                console.log('error running query');
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    },
    update: function(params, callback){
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }        
        var filSearch = {
            "discriminator":idx(request, _obj => _obj.discriminator) ,
            "username": idx(request, _obj => _obj.username) ,
        }
        collection.findOneAndUpdate(filSearch,{$set: request},{ upsert: true, returnOriginal: false },  function (err, result) {
            let event = {};
            if (err) {
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    },
    updateRtk: function(params, callback){
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let rtk = idx(params, _obj => _obj.request.rtk);
        var filSearch = {
            "discriminator":idx(request, _obj => _obj.discriminator) ,
            "username": idx(request, _obj => _obj.username) ,
        }

        collection.findOneAndUpdate(filSearch,{$addToSet: {rtk}},{ upsert: true, returnOriginal: false },  function (err, result) {
            let event = {};
            if (err) {
            } else {
                event = idx(result, _obj => _obj.ops[0]);
            }
            callback(err, { event });
        });
    },
}