const COLLECTION_NAME = 'roles';
let _ = require('lodash');
let async = require('async');
let idx = require('idx');
var { DateTime } = require('luxon');

let database = require('../database');

module.exports = {
    getList: function (params, callback) {
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request) || {};
        let pageNo = idx(params, _obj => _obj.pageNo) || 1;
        let sortBy = idx(params, _obj => _obj.sortBy) || { };
        let pageSize = idx(params, _obj => _obj.pageSize) || 1000000;
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }
        request = Object.assign(request, {deleted: false });

        async.parallel({
            activities: function (cb) {
                let skipPage = (pageNo - 1) * pageSize;
                collection
                    .find(request)
                    .sort(sortBy)
                    .skip(skipPage)
                    .limit(pageSize)
                    .toArray(function (err, results) {
                        if (err) {
                        } else {
                            results = _.map(results, activity => {
                                return _.omit(activity, filterKey);
                            });
                        }
                        cb(err, results)
                    });
            },
            totalCounts: function (cb) {
                collection.count(request, function (err, count) {
                    let pageCount = 0;
                    if (err) {
                    } else {
                        pageCount = Math.ceil(count / pageSize);
                    }
                    cb(err, { pageCount, count })
                });
            }
        }, function (err, results) {
            let activities = idx(results, _obj => _obj.activities) || [];
            let pageCount = idx(results, _obj => _obj.totalCounts.pageCount) || 0;
            let recordCount = idx(results, _obj => _obj.totalCounts.count) || 0;
            callback(err, { activities, pageCount, recordCount });
        });

    },
    create: function (params, callback) {
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }

        collection.insertOne(request, function (err, result) {
            let event = {};
            if (err) {
                logger.error('error running query', err);
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    },
    update: function(params, callback){
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }

        
        var filSearch = {
            "roleId":idx(request, _obj => _obj.roleId) 
        }
        collection.findOneAndUpdate(filSearch,{$set: request},{ upsert: true, returnOriginal: false },  function (err, result) {
            let event = {};
            if (err) {
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    }
}