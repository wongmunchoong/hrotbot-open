const COLLECTION_NAME = 'sessions';
let _ = require('lodash');
let async = require('async');
let idx = require('idx');
var { DateTime } = require('luxon');

let database = require('../database');

module.exports = {
    getList: function (params, callback) {
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request) || {};
        let sortBy = idx(params, _obj => _obj.sortBy) || { };
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }
        request = {};

        collection
        .find(request)
        .sort(sortBy)
        .toArray(function (err, results) {
            if (err) {
            } else {
                results = _.map(results, activity => {
                    return _.omit(activity, filterKey);
                });
            }
            callback(err, results)
        });

    },
    create: function (params, callback) {
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }

        collection.insertOne(request, function (err, result) {
            let event = {};
            if (err) {
                logger.error('error running query', err);
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    },
    update: function(params, callback){
        let collection = database.collection(COLLECTION_NAME);
        let request = idx(params, _obj => _obj.request);
        let filterKey = idx(params, _obj => _obj.omit) || [];
        if (Array.isArray(filterKey)) {
            filterKey.push('deleted');
        }

        
        var filSearch = {
            "roleId":idx(request, _obj => _obj.roleId) 
        }
        collection.findOneAndUpdate(filSearch,{$set: request},{ upsert: true, returnOriginal: false },  function (err, result) {
            let event = {};
            if (err) {
            } else {
                event = idx(result, _obj => _obj.ops[0]);
                event = _.omit(event, filterKey);
            }
            callback(err, { event });
        });
    }
}