var async = require('async');
const {
  default: idx
} = require('idx');
var createError = require('http-errors');
var {
  Client
} = require('discord.js')

var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

let database = require('./database');

let memberService = require('./service/members');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var botRouter = require('./routes/bots');
var adminRouter = require('./routes/admins');


var { messageHandler }= require('./handlers/message');
var { roleHandler }= require('./handlers/role');
var { languageHandler }= require('./handlers/language');
var { scheduleHandler }= require('./handlers/schedule');

database.connect();

var app = express();
app.discord = new Client();
app.discord.token = ''

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

 
app.discord.on("message",(message) => {
  messageHandler(message,app.discord.guilds.cache.get(""));
});
app.discord.on('raw', packet => {
  if (['MESSAGE_REACTION_ADD', 'MESSAGE_REACTION_REMOVE'].includes(packet.t)) {
    if(packet.d.channel_id =="") {
      if(packet.d.message_id == ""){
        roleHandler(packet,app.discord.guilds.cache.get(""));
      }
      if(packet.d.message_id == ""){
        languageHandler(packet,app.discord.guilds.cache.get(""));
      }
      if(packet.d.message_id == ""){
        scheduleHandler(packet,app.discord.guilds.cache.get(""));
      }
    } else
    if(packet.d.channel_id =="" && packet.d.message_id == ""){
        scheduleHandler(packet,app.discord.guilds.cache.get(""));
    }else
    if (['', '','',''].includes(packet.d.channel_id)){
      if(packet.t === 'MESSAGE_REACTION_ADD' && packet.d.emoji.name === '🛎'){   
        if(packet.d.member.user.bot){
          return;
        }
        app.discord.guilds.cache.get("").members.cache.get(packet.d.user_id).send("Meow! Is there anything i can assist you? (PS: All inquiry, feedback and request will be made available to Moderators)");
        app.discord.guilds.cache.get("").channels.cache.get(packet.d.channel_id).messages.fetch({ around:packet.d.message_id, limit: 1 })
        .then(msg => {msg.first().reactions.cache.get('🛎').users.remove(packet.d.user_id); });
      }
    }
  }
});

app.use(function (req, res, next) {
  async.waterfall([function (callback) {
    const ds = app.discord.guilds.cache.get("");

    callback(null, ds)
  }], function (error, result) {
    req.discord = result;
    next()
  })
});

app.use(function (req, res, next) {
  res.locals.title = "[Elemental] DRS Casual";
  res.locals.activeSidebar = "index";
  res.locals.fullUri = req.protocol + '://' + req.get('host') + req.originalUrl;
  res.locals.baseUri = req.protocol + '://' + req.get('host');
  req.BaseUri = req.protocol + '://' + req.get('host');
  req.accessToken = req.cookies.atk;
  req.refreshToken = req.cookies.rtk;

  if (req.refreshToken) {
    memberService.getDetail({
      request: {
        rtk: req.refreshToken
      }
    }, function (err, record) {
      var currentUser = {
        discriminator: idx(record, _obj => _obj.user.discriminator),
        username: idx(record, _obj => _obj.user.username),
        avatarId: idx(record, _obj => _obj.user.avatarId),
        guildID: idx(record, _obj => _obj.user.guildID),
        nickname: idx(record, _obj => _obj.user.nickname),
        roles: idx(record, _obj => _obj.user.roles),
        userID: idx(record, _obj => _obj.user.userID),
        lodestone: {
          avatar: idx(record, _obj => _obj.user.lodestoneavatar),
          potrait: idx(record, _obj => _obj.user.lodestonepotrait),
          server: idx(record, _obj => _obj.user.lodestoneserver),
          userID: idx(record, _obj => _obj.user.lodestoneuserid),
          username: idx(record, _obj => _obj.user.lodestoneusername)
        }
      }
      req.currentUser = currentUser;
      res.locals.user = currentUser;
      res.admin = false;
      res.locals.admin = false;
      var roleList = idx(record, _obj => _obj.user.roles)
      if(roleList){
        if(roleList.includes("") || roleList.includes("")){
          res.locals.admin = true;
          res.admin = true;
        }
      }
      next();
    });
  } else {
    next();
  }
});

app.use('/bots', botRouter);
app.use('/users', usersRouter);
app.use('/admin', function (req, res, next) {
  if (req.refreshToken) {
    if (idx(req, _obj => _obj.currentUser.userID)) {
      if (idx(req, _obj => _obj.currentUser.lodestone.userID) && res.admin) {
        next();
      } else {
        res.redirect('/users/connect');
      }
    } else {
      res.clearCookie("atk", {
        path: '/'
      });
      res.clearCookie("rtk", {
        path: '/'
      });
      res.redirect('/users');
    }
  } else {
    res.redirect('/users');
  }
}, adminRouter);
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;