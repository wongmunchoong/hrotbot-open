'use strict';

var async = require('async');
var mongoClient = require('mongodb').MongoClient;

module.exports = class SchoolDatabase {
    constructor(){
        this.appDB = null;
    }

    static connect() {
        mongoClient.connect(``, (err,client) => { 
            if(err){
                console.log(err.message);
            } else {
                this.appDB = client.db('ffxivpf');
                console.log('Successfully connect to APP Database');
            }
         });
    }

    static db(){
        return this.appDB;
    }

    static collection(param){
        if(this.appDB){
            return this.appDB.collection(param);
        } else {
            console.log('APP Database Not Found');
            return null;
        }
    }
}