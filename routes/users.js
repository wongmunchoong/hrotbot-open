const axios = require('axios');
const async = require('async');
const queryString = require('querystring');
var express = require('express');
var router = express.Router();

let memberService = require('../service/members');
const { default: idx } = require('idx');

const cfg = {
  id: '812957520045277204',
  secret: 'GbvVqVImxcqTM-KbU07Yg07pC4rn5NN8'
};


/* GET users listing. */
router.get('/', function(req, res, next) {
  
  if(req.accessToken){    
    res.redirect('/')
  }else {
  res.render('login');
  }
});

router.all('/logout', function(req, res, next) {
  res.clearCookie("atk",{path:'/'});
  res.clearCookie("rtk",{path:'/'});
  res.redirect('/users');  
});

router.all('/login', function(req, res, next) {
  res.redirect('https://discord.com/api/oauth2/authorize?client_id=&redirect_uri=http%3A%2F%2Fwww.ffxivpf.com%2Fusers%2Fauthorize&response_type=code&scope=identify');
  
});

/* GET users listing. */
router.get('/authorize', function(req, res, next) {
  const code = req.query.code;
  var atk ="";
  var rtk ="";
  async.waterfall([
    function(callback){
      
      axios.post('https://discordapp.com/api/oauth2/token', queryString.stringify({
        'client_id': cfg.id,
        'client_secret': cfg.secret,
        'grant_type': 'authorization_code',
        'code': code,
        'redirect_uri': decodeURIComponent(`${req.BaseUri}/users/authorize`),
        'scope': 'identify'
    }), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    }).then(response => {
        console.log(`statusCode: ${response.statusCode}`)
        console.log(response)
        let config =  {maxAge:'/'};
        config.maxAge = 91556952000;        
        atk =response.data.access_token;
        rtk =response.data.refresh_token;
        res.cookie("atk", response.data.access_token, config);
        res.cookie("rtk", response.data.refresh_token, config);
        //res.redirect(`/users/guilds?token=${response.data.access_token}`);
        callback(null, response);
      })
      .catch(error => {
        console.error(error);
        callback(error);
      });
    }, function(r, callback){
      axios({
        method:'get',
        url:'https://discord.com/api/v8/users/@me',
        headers:{
          Authorization:`Bearer ${r.data.access_token}`
        }   
      }).then(response => {
        console.log(`statusCode: ${response.statusCode}`)
        console.log(response);
        callback(null,response.data);
      })
      .catch(error => {
        console.error(error);
        callback(error);
      })
    }, function(data,callback){
      memberService.getDetail({request:{userID:data.id}},function(err,record){
        if(idx(record, _obj => _obj.userID)){   
          var ret = {            
            "username": idx(record, _obj => _obj.username) ,
            "discriminator":idx(record, _obj => _obj.discriminator) ,
            rtk
          }       
          callback(null,ret);
        }else{
          var user = req.discord.members.cache.get(data.id);
          if(user){
            console.log(user);
            memberService.update({
              request:{        
                "guildID":idx(user, _obj => _obj.id),
                "deleted": false,
                "avatarId":idx(user, _obj => _obj.user.avatar),
                "nickname": idx(user, _obj => _obj.nickname),
                "userID":idx(user, _obj => _obj.user.id),
                "discriminator":idx(user, _obj => _obj.user.discriminator) ,
                "username": idx(user, _obj => _obj.user.username) ,
                "roles": idx(user, _obj => _obj._roles) 
              }}, (err,result)=>{
                var ret = {            
                  "username": idx(user, _obj => _obj.user.username) ,
                  "discriminator":idx(user, _obj => _obj.user.discriminator),
                  rtk
                }       
                callback(null,ret);
              });
          } else {            
            res.clearCookie("atk",{path:'/'});
            res.clearCookie("rtk",{path:'/'});
            res.render('connect',{userID:data.id, avatarId: data.avatar,discriminator: data.discriminator , username:data.username});
          }
        }
      });
    }, function(data,callback){
      memberService.updateRtk({request:data},callback);
    }
  ],function(error,data){
    if(error){      
      res.clearCookie("atk",{path:'/'});
      res.clearCookie("rtk",{path:'/'});
      res.redirect(`/`);
    }else{
      res.redirect(`/`);
    }
  })
});

/* GET users listing. */
router.get('/connect', function(req, res, next) {
  if(req.accessToken){    
    memberService.getDetail({request:{rtk:req.refreshToken}},function(err,record){
      if(idx(record, _obj => _obj.user.lodestoneuserid)){
        res.redirect("/");
      } else{
        res.render('lodestone');
      }
    });
  } else {
    res.redirect("/");
  }
});

/* GET users listing. */
router.post('/connect', function(req, res, next) {
  if(req.accessToken){    
    memberService.getDetail({request:{rtk:req.refreshToken}},function(err,record){
      if(idx(record, _obj => _obj.user.lodestoneuserid)){
        res.json({});
      } else{
        var request={
           discriminator:idx(record, _obj => _obj.user.discriminator),
           username: idx(record, _obj => _obj.user.username) ,
           lodestoneavatar:idx(req, _obj => _obj.body.avatar) ,
           lodestonepotrait:idx(req, _obj => _obj.body.potrait),
           lodestoneserver:idx(req, _obj => _obj.body.server),
           lodestoneuserid:idx(req, _obj => _obj.body.userid),
           lodestoneusername:idx(req, _obj => _obj.body.username)
         }
         memberService.update({request}, (err,result)=>{res.json(req.body)});
      }

    });
  } else {
    res.json({});
  }
});
module.exports = router;
