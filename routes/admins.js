var express = require('express');
var { DateTime } = require('luxon');
var router = express.Router();
let sessionService = require('../service/sessions');

/* GET home page. */
router.get('/', function(req, res, next) {
  sessionService.getList({},function(err,data){
    console.log(data)
    res.render('admin', data);

  });
});

router.post('/create', function(req, res, next) {
  var { title, desc,ment, eventDate, eventTime, bossLevel, dutyLevel} = req.body;  
  var newDate = DateTime.fromFormat(eventDate+ " "+ eventTime, "dd/MM/yyyy t");
  console.log(newDate.toLocaleString());
  sessionService.create({
    request:{        
      title, description:desc, mentor:ment, sessionOn:newDate, bossLevel, dutyLevel
    }}, (err,result)=>{
      res.json(result);
    });
});

module.exports = router;