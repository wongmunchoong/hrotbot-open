var express = require('express');
var async = require('async');
const Discord = require('discord.js');
let idx = require('idx');
var router = express.Router();

const EorzeaWeather = require('eorzea-weather');
let {
  weatherHandler,
  weatherIconHandler
} = require('../handlers/weather');

let memberService = require('../service/members');
let roleService = require('../service/roles');

const tankEmoji = "829918310635143230";
const healerEmoji = "829918311185514537";
const meleeEmoji = "829918310648643605";
const rdpsEmoji = "829918311093108746";
const casterEmoji = "829918310740787251";

/* GET home page. */
router.get('/', function (req, res, next) {
  var list = req.discord.members.cache;
  var record = [{}];
  console.log(list);
  list.forEach(l =>
    memberService.update({
      request: {
        "guildID": idx(l, _obj => _obj.id),
        "deleted": false,
        "avatarId": idx(l, _obj => _obj.user.avatar),
        "nickname": idx(l, _obj => _obj.nickname),
        "userID": idx(l, _obj => _obj.user.id),
        "discriminator": idx(l, _obj => _obj.user.discriminator),
        "username": idx(l, _obj => _obj.user.username),
        "roles": idx(l, _obj => _obj._roles)
      }
    }, function (err, rest) {}));


  res.json(list);
});

router.get('/message', function (req, res, next) {
  // req.discord.members.cache.get('').send(' Meow~~~~');
  // res.json({});

  req.discord.channels.cache.get('').messages.fetch({ })
  .then(msg => {
      msg.forEach(element => {
        element.react("🛎");    
      });
  });
res.json({});
})


router.get('/impersonate', function (req, res, next) {
  var member = req.discord.members.cache.get('');
  req.discord.channels.cache.get('').createWebhook(member.nickname, {
    avatar: `https://cdn.discordapp.com/avatars/${member.id}/${member.user.avatar}.png`,
  })  
	.then(webhook => {
    webhook.send("<@185035723437637642> I WANT YOU!!!")
  });
  res.json(member);
})

router.get('/message/:id', function (req, res, next) {
  req.discord.channels.cache.get('').messages.fetch({ })
    .then(msg => {
        msg.forEach(element => {
          req.discord.members.cache.get(req.params.id).send(element.content);          
        });
    });
  res.json({});
})

router.get('/init2', function (req, res, next) {
  req.discord.channels.cache.get('').messages.fetch({
    around: '',
    limit: 1
  })
  .then(msg => {
    const fetchedMsg = msg.first();
    fetchedMsg.react("🌱")
      .then(() => fetchedMsg.react("1️⃣"))
      .then(() => fetchedMsg.react("2️⃣"))
      .then(() => fetchedMsg.react("3️⃣"))
      .then(() => fetchedMsg.react("4️⃣"))
      .then(() => fetchedMsg.react("🔁"))
      .then(() => fetchedMsg.react("📨"))
      .catch((e) => {
        console.error(e.message)
      });
    });
  res.json({});
})

router.get('/init', function (req, res, next) {

  // const embed = new Discord.MessageEmbed()
  //   .setColor('#edd028')
  //   .setTitle('Introduce yourself')
  //   .setDescription('Please write your home server together with your character full name and i will search lodestone for your record and rename your nickname according to lodestone.')
  //   .setThumbnail('https://ffxiv.gamerescape.com/w/images/6/6b/MainIcon43.png')
  //   .setImage('https://s3.gifyu.com/images/ezgif.com-gif-maker-4.gif')
  //   .setFooter('[SERVER]  [FIRST NAME]  [LAST NAME]', 'https://ffxiv.gamerescape.com/w/images/f/fb/MainIcon30.png');
  
  const embed = new Discord.MessageEmbed()
    .setColor('#9cb900')
    .setTitle('Schedule Notification')
    .setDescription('Please select the progression you wish to be inform in.')
    .setThumbnail('https://ffxiv.gamerescape.com/w/images/b/bc/MainIcon80.png')
    .addFields({
      name: "🌱 - Fresh",
      value: '\u200b'
    })
    .addFields({
      name: "1️⃣ - Boss 1 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "2️⃣ - Boss 2 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "3️⃣ - Boss 3 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "4️⃣ - Boss 4 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "🔁 - Reclear",
      value: '\u200b'
    })    
    .addFields({
      name: "📚 - Classroom",
      value: '\u200b'
    })    
    .addFields({
      name: "📨 - Direct Message (Experimental)",
      value: '\u200b'
    })
    .setFooter('Please choose your schedule Notification', 'https://ffxiv.gamerescape.com/w/images/9/92/Player25_Icon.png');

  //req.discord.channels.cache.get('829748472713576538').send(embed);
 /*  
 req.discord.channels.cache.get('829748472713576538').messages.fetch({ around: '874358552620302447', limit: 1 })
  .then(msg => {
      const fetchedMsg = msg.first();
      fetchedMsg.edit(embed);

  });
  */
  req.discord.channels.cache.get('').messages.fetch({ around: '', limit: 1 })
    .then(msg => {
        const fetchedMsg = msg.first();
        fetchedMsg.edit(embed);
    fetchedMsg.react("🌱")
    .then(() => fetchedMsg.react("1️⃣"))
    .then(() => fetchedMsg.react("2️⃣"))
    .then(() => fetchedMsg.react("3️⃣"))
    .then(() => fetchedMsg.react("4️⃣"))
    .then(() => fetchedMsg.react("🔁"))
    .then(() => fetchedMsg.react("📚"))
    .then(() => fetchedMsg.react("📨"))
          .catch((e) => {
            console.error(e.message)
          });
    });
   
  res.json({});
  
})

router.get('/test', function (req, res, next) {

  // const embed = new Discord.MessageEmbed()
  //   .setColor('#edd028')
  //   .setTitle('Introduce yourself')
  //   .setDescription('Please write your home server together with your character full name and i will search lodestone for your record and rename your nickname according to lodestone.')
  //   .setThumbnail('https://ffxiv.gamerescape.com/w/images/6/6b/MainIcon43.png')
  //   .setImage('https://s3.gifyu.com/images/ezgif.com-gif-maker-4.gif')
  //   .setFooter('[SERVER]  [FIRST NAME]  [LAST NAME]', 'https://ffxiv.gamerescape.com/w/images/f/fb/MainIcon30.png');
  
  const embed = new Discord.MessageEmbed()
    .setColor('#9cb900')
    .setTitle('Schedule Notification')
    .setDescription('Please head over to [Setting Channel](https://discord.com/channels/809448172988006440/829748472713576538/891945795811696640) to opt in for notification ping or [click here!](https://discord.com/channels/809448172988006440/829748472713576538/891945795811696640)')
    .setThumbnail('https://ffxiv.gamerescape.com/w/images/b/bc/MainIcon80.png')
/*
    .addFields({
      name: "🌱 - Fresh",
      value: '\u200b'
    })
    .addFields({
      name: "1️⃣ - Boss 1 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "2️⃣ - Boss 2 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "3️⃣ - Boss 3 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "4️⃣ - Boss 4 (Practice/Aim to Clear)",
      value: '\u200b'
    })
    .addFields({
      name: "🔁 - Reclear",
      value: '\u200b'
    })    
    .addFields({
      name: "📚 - Classroom",
      value: '\u200b'
    })    
    .addFields({
      name: "📨 - Direct Message (Experimental)",
      value: '\u200b'
    })
    .setFooter('Please choose your schedule Notification', 'https://ffxiv.gamerescape.com/w/images/9/92/Player25_Icon.png')
    */
    ;

  //req.discord.channels.cache.get('874343338751447110').send(embed);
  req.discord.channels.cache.get('823166335285723136').messages.fetch({ around: '874358552620302447', limit: 1 })
  .then(msg => {
      const fetchedMsg = msg.first();
      fetchedMsg.edit(embed);

  });
  /*
  req.discord.channels.cache.get('874343338751447110').messages.fetch({ around: '874354508648562718', limit: 1 })
    .then(msg => {
        const fetchedMsg = msg.first();
        fetchedMsg.edit(embed);
    fetchedMsg.react("🌱")
    .then(() => fetchedMsg.react("1️⃣"))
    .then(() => fetchedMsg.react("2️⃣"))
    .then(() => fetchedMsg.react("3️⃣"))
    .then(() => fetchedMsg.react("4️⃣"))
    .then(() => fetchedMsg.react("🔁"))
    .then(() => fetchedMsg.react("📚"))
    .then(() => fetchedMsg.react("📨"))
          .catch((e) => {
            console.error(e.message)
          });
    });
    */
  res.json({});
})
router.get('/roles', function (req, res, next) {
  var list = req.discord.roles.cache;
  console.log(list);
  list.forEach(l =>
    roleService.update({
      request: {
        "roleId": idx(l, _obj => _obj.id),
        "guildID": idx(l, _obj => _obj.guild.id),
        "name": idx(l, _obj => _obj.name),
        "deleted": false,
      }
    }, function (err, rest) {}));

  res.json(list);
});

router.get('/cron', function (req, res, next) {
    req.discord.channels.cache.get('').messages.fetch({
        limit: 99
      })
      .then(msg => {
        msg.forEach(element => {
          element.delete().then(data => {
            //console.log(data);
          })
        });
      });
      req.discord.channels.cache.get('').messages.fetch({
          limit: 99
        })
        .then(msg => {
          msg.forEach(element => {
            element.delete().then(data => {
              //console.log(data);
            })
          });
        });

        async.parallel([
          function(callback) {
            const eorzeaWeather = weatherHandler(EorzeaWeather.ZONE_BOZJAN_SOUTHERN_FRONT);
            async.eachOfSeries(eorzeaWeather, (item, index, cb) => {
              var indicator = (index + 1) % 3;
              var etTxt = 'Eorzea Time 16:00 -- 23:59'
              if (indicator == 1) {
                etTxt = 'Eorzea Time 00:00 -- 07:59'
              } else if (indicator == 2) {
                etTxt = 'Eorzea Time 08:00 --15:59'
              }
              const embed = new Discord.MessageEmbed()
                .setColor('#edd028')
                .setTitle(etTxt)
                .setAuthor(item.name, weatherIconHandler(item.name))
                .setTimestamp(item.startedAt)
              ;
      
              req.discord.channels.cache.get('').send(embed);
              cb();
            },
            function (err) {
              callback(err,true);
            });
          },
          function(callback) {
            const eorzeaWeather = weatherHandler(EorzeaWeather.ZONE_ZADNOR);
            async.eachOfSeries(eorzeaWeather, (item, index, cb) => {
              var indicator = (index + 1) % 3;
              var etTxt = 'Eorzea Time 16:00 -- 23:59'
              if (indicator == 1) {
                etTxt = 'Eorzea Time 00:00 -- 07:59'
              } else if (indicator == 2) {
                etTxt = 'Eorzea Time 08:00 --15:59'
              }
              const embed = new Discord.MessageEmbed()
                .setColor('#edd028')
                .setTitle(etTxt)
                .setAuthor(item.name, weatherIconHandler(item.name))
                .setTimestamp(item.startedAt)
              ;
      
              req.discord.channels.cache.get('').send(embed);
              cb();
            },
            function (err) {
              callback(err,true);
            });
          }
      ],
      // optional callback
      function(err, results) {
        console.log('all done!!!');
        res.json({});
      });
     
});


router.get('/weather/force', function (req, res, next) {
    req.discord.channels.cache.get('').messages.fetch({
        limit: 99
      })
      .then(msg => {
        msg.forEach(element => {
          element.delete().then(data => {
            //console.log(data);
          })
        });
      });
      req.discord.channels.cache.get('').messages.fetch({
          limit: 99
        })
        .then(msg => {
          msg.forEach(element => {
            element.delete().then(data => {
              //console.log(data);
            })
          });
        });

        async.parallel([
          function(callback) {
            const eorzeaWeather = weatherHandler(EorzeaWeather.ZONE_BOZJAN_SOUTHERN_FRONT);
            async.eachOfSeries(eorzeaWeather, (item, index, cb) => {
              var indicator = (index + 1) % 3;
              var etTxt = 'Eorzea Time 16:00 -- 23:59'
              if (indicator == 1) {
                etTxt = 'Eorzea Time 00:00 -- 07:59'
              } else if (indicator == 2) {
                etTxt = 'Eorzea Time 08:00 --15:59'
              }
              const embed = new Discord.MessageEmbed()
                .setColor('#edd028')
                .setTitle(etTxt)
                .setAuthor(item.name, weatherIconHandler(item.name))
                .setTimestamp(item.startedAt)
              ;
      
              req.discord.channels.cache.get('').send(embed);
              cb();
            },
            function (err) {
              callback(err,true);
            });
          },
          function(callback) {
            const eorzeaWeather = weatherHandler(EorzeaWeather.ZONE_ZADNOR);
            async.eachOfSeries(eorzeaWeather, (item, index, cb) => {
              var indicator = (index + 1) % 3;
              var etTxt = 'Eorzea Time 16:00 -- 23:59'
              if (indicator == 1) {
                etTxt = 'Eorzea Time 00:00 -- 07:59'
              } else if (indicator == 2) {
                etTxt = 'Eorzea Time 08:00 --15:59'
              }
              const embed = new Discord.MessageEmbed()
                .setColor('#edd028')
                .setTitle(etTxt)
                .setAuthor(item.name, weatherIconHandler(item.name))
                .setTimestamp(item.startedAt)
              ;
      
              req.discord.channels.cache.get('').send(embed);
              cb();
            },
            function (err) {
              callback(err,true);
            });
          }
      ],
      // optional callback
      function(err, results) {
        console.log('all done!!!');
        res.json({});
      });
     
});

router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  var memberRole = req.discord.roles.cache.find(r => r.name === "practice");
  let member = req.discord.members.cache.get(id)
  member.roles.add(memberRole);
  res.json({});
});

router.get('/:id/remove', function (req, res, next) {
  var id = req.params.id;
  var memberRole = req.discord.roles.cache.find(r => r.name === "practice");
  let member = req.discord.members.cache.get(id)
  member.roles.remove(memberRole);
  res.json({});
});

module.exports = router;